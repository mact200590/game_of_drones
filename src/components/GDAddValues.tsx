import { makeStyles } from "@material-ui/styles";
import React, { useMemo, useState } from "react";
import { GDButton } from "./GDButton";
import { GDInput } from "./GDInput";

export type GDAddValuesType = "primary" | "secondary";
interface Props {
  textPlaceHolder: string;
  labelButton: string;
  onClick?: (value: string) => void;
  typeVariant?: GDAddValuesType;
}

const GDAddValues = ({
  textPlaceHolder,
  onClick,
  labelButton,
  typeVariant = "primary"
}: Props) => {
  const classes = useStyles();
  const [value, setValue] = useState("");
  const disable = useMemo(() => value === "", [value]);
  const handleOnchange = (e: any) => {
    setValue(e.target.value);
  };

  return (
    <div className={classes.container}>
      <GDInput
        typeVariant={typeVariant}
        margin="normal"
        fullWidth={false}
        label={textPlaceHolder}
        autoFocus
        value={value}
        onChange={handleOnchange}
      />
      <GDButton
        disabled={disable}
        className={classes.button}
        label={labelButton}
        typeVariant={typeVariant}
        fullWidth={false}
        type="submit"
        onClick={() => {
          !disable && onClick && onClick(value);
          !disable && setValue("");
        }}
      />
    </div>
  );
};
export default GDAddValues;

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexDirection: "row"
  },
  button: {
    marginTop: 25,
    maxHeight: 35,
    marginLeft: 5
  }
}));
