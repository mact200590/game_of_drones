import { red } from '@material-ui/core/colors';

export const palette = {
  primary: {
    main: '#25D34D',
  },
  secondary: {
    main: '#19857b',
  },
  error: {
    main: red.A400,
  },
  background: {
    default: '#fff',
  },
}